build:
	@go build -o bin/cook-codex

run: build
	@./bin/cook-codex

test:
	@go test -v ./...
