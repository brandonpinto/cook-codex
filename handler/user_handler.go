package handler

import "github.com/gofiber/fiber/v2"

func HandlePostUser(c *fiber.Ctx) error {
	return c.JSON("POST user")
}

func HandleDeleteUser(c *fiber.Ctx) error {
	return c.JSON("DELETE user")
}

func HandleUpdateUser(c *fiber.Ctx) error {
	return c.JSON("PUT user")
}

func HandleGetUser(c *fiber.Ctx) error {
	return c.JSON("GET user")
}

func HandleGetUsers(c *fiber.Ctx) error {
	return c.JSON("GET users")
}
