package main

import (
	"gitlab.com/brandonpinto/cook-codex/config"
	"gitlab.com/brandonpinto/cook-codex/router"
)

var (
	logger *config.Logger
)

func main() {
	logger = config.GetLogger("main")

	// Initialize Configuration
	err := config.Start()
	if err != nil {
		logger.Errorf("config initialization error: %v", err)
		return
	}

	// Initialize Router
	router.Start()
}
