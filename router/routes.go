package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/brandonpinto/cook-codex/handler"
)

func routes(r *fiber.App) {
	v1 := r.Group("/api/v1")
	{
		v1.Get("/user/:id", handler.HandleGetUser)
		v1.Get("/user", handler.HandleGetUsers)
		v1.Post("/user", handler.HandlePostUser)
		v1.Put("/user", handler.HandleUpdateUser)
		v1.Delete("/user", handler.HandleDeleteUser)
	}
}
