package router

import "github.com/gofiber/fiber/v2"

func Start() {
	router := fiber.New()

	// Initialize Routes
	routes(router)

	router.Listen(":3000")
}
